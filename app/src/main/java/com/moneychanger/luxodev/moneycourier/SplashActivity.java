package com.moneychanger.luxodev.moneycourier;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;

    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Launcher);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPref=getSharedPreferences(SplashActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        alertDialogBuilder=new AlertDialog.Builder(SplashActivity.this);
        username=sharedPref.getString("username",null);

        if(username!=null){
            CourierStatusCheck();
        }else{
            Intent intentLogin = new Intent(SplashActivity.this,LoginActivity.class);
            intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentLogin);
        }
    }

    public void BuildCloseAppDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                        System.exit(0);
                    }
                });
        alertDialog.show();
    }

    public void BuildErrorLoginDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        SharedPreferences.Editor editor=sharedPref.edit();
                        editor.remove("id");
                        editor.remove("username");
                        editor.apply();
                        
                        Intent intentLogin = new Intent(SplashActivity.this,LoginActivity.class);
                        intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentLogin);
                    }
                });
        alertDialog.show();
    }

    public void CourierStatusCheck() {
        Call<Void> courierStatusCall = apiInterface.GetRequestCourierStatus(username);
        courierStatusCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code()==200){
                    Intent intentMain = new Intent(SplashActivity.this,MainActivity.class);
                    intentMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentMain);
                }else if(response.code()==473){
                    BuildErrorLoginDialog(SplashActivity.this.getString(R.string.error_title_account_not_found), SplashActivity.this.getString(R.string.error_message_account_not_found), "Ok");
                }else if(response.code()==471){
                    BuildErrorLoginDialog(SplashActivity.this.getString(R.string.error_title_suspended), SplashActivity.this.getString(R.string.error_message_suspended), "Ok");
                }else{
                    BuildCloseAppDialog(SplashActivity.this.getString(R.string.error_title_server_error), SplashActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                call.cancel();
                BuildCloseAppDialog(SplashActivity.this.getString(R.string.error_title_internet), SplashActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }
}
