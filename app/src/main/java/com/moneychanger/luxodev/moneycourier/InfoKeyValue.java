package com.moneychanger.luxodev.moneycourier;

public class InfoKeyValue {
    public String infoKey;
    public String infoValue;

    public InfoKeyValue(String infoKey, String infoValue){
        this.infoKey=infoKey;
        this.infoValue=infoValue;
    }
}
