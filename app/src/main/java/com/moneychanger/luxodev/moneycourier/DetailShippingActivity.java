package com.moneychanger.luxodev.moneycourier;

import android.content.DialogInterface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.moneychanger.luxodev.moneycourier.pojo.response.DetailShipping;
import com.moneychanger.luxodev.moneycourier.pojo.response.Shipping;
import com.moneychanger.luxodev.moneycourier.pojo.response.Wrapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailShippingActivity extends AppCompatActivity {

    public Integer orderId;

    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;

    SwipeRefreshLayout swipeRefresh;
    ProgressBar spinner;

    RecyclerView rvDSOrderInfo;
    RecyclerView rvDSDetailOrderInfo;
    OrderInfoAdapter customOrderInfoAdapter;
    DetailOrderInfoAdapter customDetailOrderInfoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_shipping);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle=getIntent().getExtras();
        getSupportActionBar().setTitle(bundle.getString("current_order_number",""));
        orderId=bundle.getInt("current_order_id",0);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        alertDialogBuilder=new AlertDialog.Builder(DetailShippingActivity.this);

        RecyclerView.LayoutManager mLayoutManagerOrderInfo = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerDetailOrderInfo = new LinearLayoutManager(getApplicationContext());
        rvDSOrderInfo=findViewById(R.id.rvDSOrderInfo);
        customOrderInfoAdapter = new OrderInfoAdapter(new ArrayList<InfoKeyValue>());
        rvDSOrderInfo.setLayoutManager(mLayoutManagerOrderInfo);
        rvDSOrderInfo.setItemAnimator(new DefaultItemAnimator());
        rvDSOrderInfo.setAdapter(customOrderInfoAdapter);
        rvDSDetailOrderInfo=findViewById(R.id.rvDSDetailOrderInfo);
        customDetailOrderInfoAdapter = new DetailOrderInfoAdapter(new ArrayList<DetailShipping>());
        rvDSDetailOrderInfo.setLayoutManager(mLayoutManagerDetailOrderInfo);
        rvDSDetailOrderInfo.setItemAnimator(new DefaultItemAnimator());
        rvDSDetailOrderInfo.setAdapter(customDetailOrderInfoAdapter);

        spinner = findViewById(R.id.progressBarDetailShipping);
        spinner.setVisibility(View.GONE);

        swipeRefresh=findViewById(R.id.swipeRefreshDetailShipping);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        RequestDetailShipping(true);
                    }
                }
        );

        RequestDetailShipping(false);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void RequestDetailShipping(final boolean isRefresh){
        if(!isRefresh) spinner.setVisibility(View.VISIBLE);
        Call<Wrapper.ShippingDetailWrapper> detailShippingCall = apiInterface.GetRequestShippingDetail(orderId);
        detailShippingCall.enqueue(new Callback<Wrapper.ShippingDetailWrapper>() {
            @Override
            public void onResponse(Call<Wrapper.ShippingDetailWrapper> call, Response<Wrapper.ShippingDetailWrapper> response) {
                if(response.code()==200) {
                    Wrapper.ShippingDetailWrapper apiRes = response.body();
                    customOrderInfoAdapter.UpdateDataSet(BuildOrderInfoList(apiRes.shipping));
                    customDetailOrderInfoAdapter.UpdateDataSet(apiRes.detailShipping);
                }else if(response.code()==480){
                    BuildSimpleDialog(DetailShippingActivity.this.getString(R.string.error_title_unauthorized), DetailShippingActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(DetailShippingActivity.this.getString(R.string.error_title_server_error), DetailShippingActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Wrapper.ShippingDetailWrapper> call, Throwable t) {
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
                BuildSimpleDialog(DetailShippingActivity.this.getString(R.string.error_title_internet), DetailShippingActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public List<InfoKeyValue> BuildOrderInfoList(Shipping shipping){
        List<InfoKeyValue> ifkList=new ArrayList<>();
        ifkList.add(new InfoKeyValue("Order Number", shipping.orderNumber));
        ifkList.add(new InfoKeyValue("Name", shipping.name));
        ifkList.add(new InfoKeyValue("Address", shipping.address));
        ifkList.add(new InfoKeyValue("Date", shipping.date));
        return ifkList;
    }

    public class OrderInfoAdapter extends RecyclerView.Adapter<OrderInfoAdapter.OrderInfoViewHolder> {

        private List<InfoKeyValue> adapterInfoKeyValueListOrder;

        public class OrderInfoViewHolder extends RecyclerView.ViewHolder {
            public TextView textIFKKey, textIFKValue;

            public OrderInfoViewHolder(View view) {
                super(view);
                textIFKKey = view.findViewById(R.id.textIFKKey);
                textIFKValue = view.findViewById(R.id.textIFKValue);
            }
        }

        public void UpdateDataSet(List<InfoKeyValue> adapterInfoKeyValueListOrder){
            this.adapterInfoKeyValueListOrder.clear();
            this.adapterInfoKeyValueListOrder = adapterInfoKeyValueListOrder;
            this.notifyDataSetChanged();
        }

        public OrderInfoAdapter(List<InfoKeyValue> adapterInfoKeyValueListOrder) {
            this.adapterInfoKeyValueListOrder = adapterInfoKeyValueListOrder;
        }

        @Override
        public OrderInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_infokeyvalue, parent, false);

            return new OrderInfoViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final OrderInfoViewHolder holder, final int position) {
            InfoKeyValue ifkRow = adapterInfoKeyValueListOrder.get(position);
            holder.textIFKKey.setText(ifkRow.infoKey);
            holder.textIFKValue.setText(ifkRow.infoValue);
        }

        @Override
        public int getItemCount() {
            return this.adapterInfoKeyValueListOrder.size();
        }
    }

    public class DetailOrderInfoAdapter extends RecyclerView.Adapter<DetailOrderInfoAdapter.DetailOrderInfoViewHolder> {

        private List<DetailShipping> adapterDetailOrderInfoList;

        public class DetailOrderInfoViewHolder extends RecyclerView.ViewHolder {
            public TextView textDONumber, textDOAmount;

            public DetailOrderInfoViewHolder(View view) {
                super(view);
                textDONumber = view.findViewById(R.id.textDONumber);
                textDOAmount = view.findViewById(R.id.textDOAmount);
            }
        }

        public DetailOrderInfoAdapter(List<DetailShipping> adapterDetailOrderInfoList) {
            this.adapterDetailOrderInfoList = adapterDetailOrderInfoList;
        }

        public void UpdateDataSet(List<DetailShipping> adapterDetailOrderInfoList){
            this.adapterDetailOrderInfoList.clear();
            this.adapterDetailOrderInfoList = adapterDetailOrderInfoList;
            this.notifyDataSetChanged();
        }

        @Override
        public DetailOrderInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_detailorderinfo, parent, false);

            return new DetailOrderInfoViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final DetailOrderInfoViewHolder holder, final int position) {
            DetailShipping dsRow = adapterDetailOrderInfoList.get(position);
            holder.textDONumber.setText(Integer.toString(position+1)+".");
            holder.textDOAmount.setText(Integer.toString(dsRow.amount)+" X "+dsRow.code+Integer.toString(dsRow.nominal));
        }

        @Override
        public int getItemCount() {
            return adapterDetailOrderInfoList.size();
        }
    }
}
