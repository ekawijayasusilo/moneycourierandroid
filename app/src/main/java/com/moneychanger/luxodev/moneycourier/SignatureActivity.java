package com.moneychanger.luxodev.moneycourier;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;

import com.android.graphics.CanvasView;
import com.moneychanger.luxodev.moneycourier.database.DatabaseHelper;

public class SignatureActivity extends AppCompatActivity {

    CanvasView canvas = null;
    DatabaseHelper db;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);

        db = new DatabaseHelper(this);
        sharedPref=getSharedPreferences(SignatureActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        editor=sharedPref.edit();

        DisplayMetrics displayMetrics=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int newWidth=(int)(displayMetrics.widthPixels*0.5);
        int newHeight=(int)(displayMetrics.heightPixels*0.5);

        getWindow().setLayout(newWidth,newHeight);

        this.canvas = this.findViewById(R.id.canvas);
        this.canvas.setMode(CanvasView.Mode.DRAW);
        this.canvas.setDrawer(CanvasView.Drawer.PEN);
    }

    public void ClearCanvas(View view){
        this.canvas.clear();
    }

    public void SaveSignature(View view){
        byte[] bytes = this.canvas.getBitmapAsByteArray(Bitmap.CompressFormat.JPEG, 50);
        String base64Signature= Base64.encodeToString(bytes, Base64.DEFAULT);
        Long signatureId=sharedPref.getLong("signature_id",-1);
        if(signatureId==-1){
            editor.putLong("signature_id",db.insertSignatureImage(base64Signature));
            editor.apply();
        }else{
            db.updateSignatureImage(signatureId,base64Signature);
        }
        Intent intent=new Intent();
        intent.putExtra("success",true);
        setResult(2,intent);
        finish();
    }
}
