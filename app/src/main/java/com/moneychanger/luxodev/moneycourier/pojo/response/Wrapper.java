package com.moneychanger.luxodev.moneycourier.pojo.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by eka wijaya susilo on 4/8/2018.
 */

public class Wrapper {
    public class LoginWrapper {
        @SerializedName("courierData")
        public Courier courierData;
    }
    public class ShippingListWrapper {
        @SerializedName("shippingList")
        public List<Shipping> shippingList;
    }
    public class ShippingDetailWrapper {
        @SerializedName("shipping")
        public Shipping shipping;
        @SerializedName("detailShipping")
        public List<DetailShipping> detailShipping;
    }
}
