package com.moneychanger.luxodev.moneycourier;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.moneychanger.luxodev.moneycourier.database.DatabaseHelper;
import com.moneychanger.luxodev.moneycourier.pojo.request.ShippingConfirmationData;
import com.moneychanger.luxodev.moneycourier.pojo.response.DetailShipping;
import com.moneychanger.luxodev.moneycourier.pojo.response.Shipping;
import com.moneychanger.luxodev.moneycourier.pojo.response.Wrapper;

import java.util.ArrayList;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class SubmitShippingActivity extends AppCompatActivity {

    Integer orderId;
    Double latitude;
    Double longitude;
    Long signatureId;
    String base64Signature;

    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    DatabaseHelper db;

    SwipeRefreshLayout swipeRefresh;
    ProgressBar spinner;
    EditText etReceiverName;
    ImageView ivSignature;

    RecyclerView rvDSOrderInfo;
    RecyclerView rvDSDetailOrderInfo;
    OrderInfoAdapter customOrderInfoAdapter;
    DetailOrderInfoAdapter customDetailOrderInfoAdapter;
    Integer ENABLE_GPS=3;
    Integer SIGNATURE=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_shipping);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SubmitShippingActivityPermissionsDispatcher.StartGPSLocationWithPermissionCheck(this);

        Bundle bundle=getIntent().getExtras();
        getSupportActionBar().setTitle(bundle.getString("current_order_number",""));
        orderId=bundle.getInt("current_order_id",0);

        sharedPref=getSharedPreferences(SubmitShippingActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        editor=sharedPref.edit();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        alertDialogBuilder=new AlertDialog.Builder(SubmitShippingActivity.this);
        db = new DatabaseHelper(this);

        RecyclerView.LayoutManager mLayoutManagerOrderInfo = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerDetailOrderInfo = new LinearLayoutManager(getApplicationContext());
        rvDSOrderInfo=findViewById(R.id.rvSSOrderInfo);
        customOrderInfoAdapter = new OrderInfoAdapter(new ArrayList<InfoKeyValue>());
        rvDSOrderInfo.setLayoutManager(mLayoutManagerOrderInfo);
        rvDSOrderInfo.setItemAnimator(new DefaultItemAnimator());
        rvDSOrderInfo.setAdapter(customOrderInfoAdapter);
        rvDSDetailOrderInfo=findViewById(R.id.rvSSDetailOrderInfo);
        customDetailOrderInfoAdapter = new DetailOrderInfoAdapter(new ArrayList<DetailShipping>());
        rvDSDetailOrderInfo.setLayoutManager(mLayoutManagerDetailOrderInfo);
        rvDSDetailOrderInfo.setItemAnimator(new DefaultItemAnimator());
        rvDSDetailOrderInfo.setAdapter(customDetailOrderInfoAdapter);

        spinner = findViewById(R.id.progressBarSubmitShipping);
        spinner.setVisibility(View.GONE);

        swipeRefresh=findViewById(R.id.swipeRefreshSubmitShipping);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        RequestDetailShipping(true);
                    }
                }
        );

        etReceiverName=findViewById(R.id.etReceiverName);
        etReceiverName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etReceiverName.setError(etReceiverName.getText().toString().trim().length() < 3 ? "Min. 3 Characters" : null);
            }
        });
        ivSignature=findViewById(R.id.ivSignature);

        RequestDetailShipping(false);
    }

    @Override
    public boolean onSupportNavigateUp(){
        SmartLocation.with(SubmitShippingActivity.this).location().stop();
        finish();
        return true;
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        SmartLocation.with(SubmitShippingActivity.this).location().stop();
        finish();
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void StartGPSLocation(){
        if(!SmartLocation.with(SubmitShippingActivity.this).location().state().isGpsAvailable()){
            Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(gpsOptionsIntent,ENABLE_GPS);
        }else {
            SmartLocation.with(SubmitShippingActivity.this).location()
                    .start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        SubmitShippingActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_location_rationale)
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForLocation() {
        Toast.makeText(this, R.string.permission_location_denied, Toast.LENGTH_SHORT).show();
    }

    public void SubmitShipping(View view){
        spinner.setVisibility(View.VISIBLE);
        String receiverName=etReceiverName.getText().toString();
        if(receiverName.trim().length() >= 3 && base64Signature!=null) {
            if (latitude == null) latitude = 1.0;
            if (longitude == null) longitude = 1.0;
            ShippingConfirmationData shippingConfirmationData=new ShippingConfirmationData(receiverName,base64Signature,latitude,longitude);
            Call<Void> shippingConfirmationCall = apiInterface.PostRequestShippingConfirmation(orderId,shippingConfirmationData);
            shippingConfirmationCall.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.code() == 200) {
                        SmartLocation.with(SubmitShippingActivity.this).location().stop();

                        Intent intentMain = new Intent(SubmitShippingActivity.this,MainActivity.class);
                        intentMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentMain);
                    } else if(response.code()==480){
                        BuildSimpleDialog(SubmitShippingActivity.this.getString(R.string.error_title_unauthorized), SubmitShippingActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                    } else {
                        BuildSimpleDialog(SubmitShippingActivity.this.getString(R.string.error_title_server_error), SubmitShippingActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                    }
                    spinner.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    call.cancel();
                    spinner.setVisibility(View.GONE);
                    BuildSimpleDialog(SubmitShippingActivity.this.getString(R.string.error_title_internet), SubmitShippingActivity.this.getString(R.string.error_message_internet), "Dismiss");
                }
            });
        }else{
            spinner.setVisibility(View.GONE);
            BuildSimpleDialog(SubmitShippingActivity.this.getString(R.string.error_title_invalid_input), SubmitShippingActivity.this.getString(R.string.error_message_invalid_input), "Dismiss");
        }
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void DrawSignature(View view){
        Intent intent=new Intent(SubmitShippingActivity.this,SignatureActivity.class);
        startActivityForResult(intent, SIGNATURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==SIGNATURE){
            Bundle bundle=data.getExtras();
            if(bundle!=null && bundle.getBoolean("success",false)) {
                base64Signature = db.getSignatureImage(sharedPref.getLong("signature_id", -1));
                byte[] decodedString = Base64.decode(base64Signature, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                ivSignature.setImageBitmap(decodedByte);
            }
        }else if(requestCode==ENABLE_GPS){
            if(SmartLocation.with(SubmitShippingActivity.this).location().state().isAnyProviderAvailable()){
                SmartLocation.with(SubmitShippingActivity.this).location().start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }
                });
            }
        }
    }

    public void RequestDetailShipping(final boolean isRefresh){
        if(!isRefresh) spinner.setVisibility(View.VISIBLE);
        Call<Wrapper.ShippingDetailWrapper> detailShippingCall = apiInterface.GetRequestShippingDetail(orderId);
        detailShippingCall.enqueue(new Callback<Wrapper.ShippingDetailWrapper>() {
            @Override
            public void onResponse(Call<Wrapper.ShippingDetailWrapper> call, Response<Wrapper.ShippingDetailWrapper> response) {
                if(response.code()==200) {
                    Wrapper.ShippingDetailWrapper apiRes = response.body();
                    customOrderInfoAdapter.UpdateDataSet(BuildOrderInfoList(apiRes.shipping));
                    customDetailOrderInfoAdapter.UpdateDataSet(apiRes.detailShipping);
                } else if(response.code()==480){
                    BuildSimpleDialog(SubmitShippingActivity.this.getString(R.string.error_title_unauthorized), SubmitShippingActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                } else{
                    BuildSimpleDialog(SubmitShippingActivity.this.getString(R.string.error_title_server_error), SubmitShippingActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Wrapper.ShippingDetailWrapper> call, Throwable t) {
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
                BuildSimpleDialog(SubmitShippingActivity.this.getString(R.string.error_title_internet), SubmitShippingActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public List<InfoKeyValue> BuildOrderInfoList(Shipping shipping){
        List<InfoKeyValue> ifkList=new ArrayList<>();
        ifkList.add(new InfoKeyValue("Order Number", shipping.orderNumber));
        ifkList.add(new InfoKeyValue("Name", shipping.name));
        ifkList.add(new InfoKeyValue("Address", shipping.address));
        ifkList.add(new InfoKeyValue("Date", shipping.date));
        return ifkList;
    }

    public class OrderInfoAdapter extends RecyclerView.Adapter<OrderInfoAdapter.OrderInfoViewHolder> {

        private List<InfoKeyValue> adapterInfoKeyValueListOrder;

        public class OrderInfoViewHolder extends RecyclerView.ViewHolder {
            public TextView textIFKKey, textIFKValue;

            public OrderInfoViewHolder(View view) {
                super(view);
                textIFKKey = view.findViewById(R.id.textIFKKey);
                textIFKValue = view.findViewById(R.id.textIFKValue);
            }
        }

        public void UpdateDataSet(List<InfoKeyValue> adapterInfoKeyValueListOrder){
            this.adapterInfoKeyValueListOrder.clear();
            this.adapterInfoKeyValueListOrder = adapterInfoKeyValueListOrder;
            this.notifyDataSetChanged();
        }

        public OrderInfoAdapter(List<InfoKeyValue> adapterInfoKeyValueListOrder) {
            this.adapterInfoKeyValueListOrder = adapterInfoKeyValueListOrder;
        }

        @Override
        public OrderInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_infokeyvalue, parent, false);

            return new OrderInfoViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final OrderInfoViewHolder holder, final int position) {
            InfoKeyValue ifkRow = adapterInfoKeyValueListOrder.get(position);
            holder.textIFKKey.setText(ifkRow.infoKey);
            holder.textIFKValue.setText(ifkRow.infoValue);
        }

        @Override
        public int getItemCount() {
            return this.adapterInfoKeyValueListOrder.size();
        }
    }

    public class DetailOrderInfoAdapter extends RecyclerView.Adapter<DetailOrderInfoAdapter.DetailOrderInfoViewHolder> {

        private List<DetailShipping> adapterDetailOrderInfoList;

        public class DetailOrderInfoViewHolder extends RecyclerView.ViewHolder {
            public TextView textDONumber, textDOAmount;

            public DetailOrderInfoViewHolder(View view) {
                super(view);
                textDONumber = view.findViewById(R.id.textDONumber);
                textDOAmount = view.findViewById(R.id.textDOAmount);
            }
        }

        public DetailOrderInfoAdapter(List<DetailShipping> adapterDetailOrderInfoList) {
            this.adapterDetailOrderInfoList = adapterDetailOrderInfoList;
        }

        public void UpdateDataSet(List<DetailShipping> adapterDetailOrderInfoList){
            this.adapterDetailOrderInfoList.clear();
            this.adapterDetailOrderInfoList = adapterDetailOrderInfoList;
            this.notifyDataSetChanged();
        }

        @Override
        public DetailOrderInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_detailorderinfo, parent, false);

            return new DetailOrderInfoViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final DetailOrderInfoViewHolder holder, final int position) {
            DetailShipping dsRow = adapterDetailOrderInfoList.get(position);
            holder.textDONumber.setText(Integer.toString(position+1)+".");
            holder.textDOAmount.setText(Integer.toString(dsRow.amount)+" X "+dsRow.code+Integer.toString(dsRow.nominal));
        }

        @Override
        public int getItemCount() {
            return adapterDetailOrderInfoList.size();
        }
    }
}
