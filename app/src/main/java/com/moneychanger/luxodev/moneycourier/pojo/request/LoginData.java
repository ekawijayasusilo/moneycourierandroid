package com.moneychanger.luxodev.moneycourier.pojo.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 4/8/2018.
 */

public class LoginData {
    @SerializedName("username")
    public String username;
    @SerializedName("imei")
    public String imei;

    public LoginData(String username, String imei){
        this.username=username;
        this.imei=imei;
    }
}
