package com.moneychanger.luxodev.moneycourier.pojo.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 4/8/2018.
 */

public class Shipping {
    @SerializedName("id")
    public Integer id;
    @SerializedName("order_number")
    public String orderNumber;
    @SerializedName("name")
    public String name;
    @SerializedName("address")
    public String address;
    @SerializedName("total")
    public Float total;
    @SerializedName("status_name")
    public String statusName;
    @SerializedName("delivery_name")
    public String deliveryName;
    @SerializedName("date")
    public String date;
}
