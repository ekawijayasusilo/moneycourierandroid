package com.moneychanger.luxodev.moneycourier;

import com.moneychanger.luxodev.moneycourier.pojo.request.LoginData;
import com.moneychanger.luxodev.moneycourier.pojo.request.ShippingConfirmationData;
import com.moneychanger.luxodev.moneycourier.pojo.response.Courier;
import com.moneychanger.luxodev.moneycourier.pojo.response.Wrapper;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    @POST("/api/kurir/login")
    Call<Wrapper.LoginWrapper> PostRequestLogin(@Body LoginData loginData);

    @GET("/api/kurir/login/status/{username}")
    Call<Void> GetRequestCourierStatus(@Path("username") String username);

    @GET("/api/kurir/shipping?")
    Call<Wrapper.ShippingListWrapper> GetRequestShippingList(@Query("courier_id") Integer courierId);

    @GET("/api/kurir/shipping/{order_id}")
    Call<Wrapper.ShippingDetailWrapper> GetRequestShippingDetail(@Path("order_id") Integer orderId);

    @POST("/api/kurir/shipping/{order_id}")
    Call<Void> PostRequestShippingConfirmation(@Path("order_id") Integer orderId, @Body ShippingConfirmationData shippingConfirmationData);

}
