package com.moneychanger.luxodev.moneycourier.pojo.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 4/8/2018.
 */

public class Courier {
    @SerializedName("id")
    public Integer id;
    @SerializedName("imei")
    public String imei;
    @SerializedName("username")
    public String username;
    @SerializedName("status")
    public Integer status;
    @SerializedName("status_name")
    public String statusName;
//    @SerializedName("created_at")
//    public String createdAt;
//    @SerializedName("updated_at")
//    public String updatedAt;
}
