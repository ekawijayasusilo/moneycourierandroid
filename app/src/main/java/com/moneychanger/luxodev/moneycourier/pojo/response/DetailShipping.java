package com.moneychanger.luxodev.moneycourier.pojo.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 4/8/2018.
 */

public class DetailShipping {
    @SerializedName("detail_order_id")
    public Integer id;
    @SerializedName("id")
    public Integer orderId;
    @SerializedName("code")
    public String code;
    @SerializedName("nominal")
    public Integer nominal;
    @SerializedName("amount")
    public Integer amount;
    @SerializedName("price")
    public Float price;
    @SerializedName("total")
    public Float total;
}
