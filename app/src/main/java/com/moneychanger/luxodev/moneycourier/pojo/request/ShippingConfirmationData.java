package com.moneychanger.luxodev.moneycourier.pojo.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 4/8/2018.
 */

public class ShippingConfirmationData {
    @SerializedName("receiver_name")
    public String receiverName;
    @SerializedName("image_receipt")
    public String imageReceipt;
    @SerializedName("latitude")
    public Double latitude;
    @SerializedName("longitude")
    public Double longitude;

    public ShippingConfirmationData(String receiverName, String imageReceipt, Double latitude, Double longitude){
        this.receiverName=receiverName;
        this.imageReceipt=imageReceipt;
        this.latitude=latitude;
        this.longitude=longitude;
    }
}
