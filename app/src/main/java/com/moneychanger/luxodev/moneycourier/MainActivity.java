package com.moneychanger.luxodev.moneycourier;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.moneychanger.luxodev.moneycourier.pojo.response.Shipping;
import com.moneychanger.luxodev.moneycourier.pojo.response.Wrapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;

    RecyclerView rvShippingList;
    ShippingListAdapter customShippingListAdapter;
    SwipeRefreshLayout swipeRefresh;
    ProgressBar spinner;

    CardView cardNoHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPref=getSharedPreferences(MainActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        alertDialogBuilder=new AlertDialog.Builder(MainActivity.this);

        cardNoHistory=findViewById(R.id.cardNoDelivery);
        cardNoHistory.setVisibility(View.GONE);

        rvShippingList=findViewById(R.id.rvShippingList);
        customShippingListAdapter = new ShippingListAdapter(new ArrayList<Shipping>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvShippingList.setLayoutManager(mLayoutManager);
        rvShippingList.setItemAnimator(new DefaultItemAnimator());
        rvShippingList.setAdapter(customShippingListAdapter);

        spinner = (ProgressBar)findViewById(R.id.progressBarShiipingList);
        spinner.setVisibility(View.GONE);
        swipeRefresh=(SwipeRefreshLayout)findViewById(R.id.swipeRefreshShippingList);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            RequestShippingList(true);
                        }
                    }
        );

        RequestShippingList(false);
    }

    public void RequestShippingList(final boolean isRefresh){
        if(!isRefresh) spinner.setVisibility(View.VISIBLE);
        Call<Wrapper.ShippingListWrapper> shippingListCall = apiInterface.GetRequestShippingList(sharedPref.getInt("id",-1));
        shippingListCall.enqueue(new Callback<Wrapper.ShippingListWrapper>() {
            @Override
            public void onResponse(Call<Wrapper.ShippingListWrapper> call, Response<Wrapper.ShippingListWrapper> response) {
                if(response.code()==200) {
                    Wrapper.ShippingListWrapper apiRes = response.body();
                    customShippingListAdapter.UpdateDataSet(apiRes.shippingList);
                }else if(response.code()==480){
                    BuildSimpleDialog(MainActivity.this.getString(R.string.error_title_unauthorized), MainActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(MainActivity.this.getString(R.string.error_title_server_error), MainActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Wrapper.ShippingListWrapper> call, Throwable t) {
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
                BuildSimpleDialog(MainActivity.this.getString(R.string.error_title_internet), MainActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public class ShippingListAdapter extends RecyclerView.Adapter<ShippingListAdapter.ShippingListViewHolder> {

        private List<Shipping> adapterShippingList;

        public class ShippingListViewHolder extends RecyclerView.ViewHolder {
            public TextView textSLOrderNumber, textSLCustomerName, textSLCustomerAddress;
            public Button btnSLView, btnSLSubmit;

            public ShippingListViewHolder(View view) {
                super(view);
                textSLOrderNumber = view.findViewById(R.id.textSLOrderNumber);
                textSLCustomerName = view.findViewById(R.id.textSLCustomerName);
                textSLCustomerAddress = view.findViewById(R.id.textSLCustomerAddress);
                btnSLView = view.findViewById(R.id.btnSLView);
                btnSLSubmit = view.findViewById(R.id.btnSLSubmit);
            }
        }

        public ShippingListAdapter(List<Shipping> adapterShippingList) {
            this.adapterShippingList = adapterShippingList;
            if(adapterShippingList.size()==0) cardNoHistory.setVisibility(View.VISIBLE);
            else cardNoHistory.setVisibility(View.GONE);
        }

        public void UpdateDataSet(List<Shipping> adapterShippingList){
            this.adapterShippingList.clear();
            this.adapterShippingList = adapterShippingList;
            if(adapterShippingList.size()==0) cardNoHistory.setVisibility(View.VISIBLE);
            else cardNoHistory.setVisibility(View.GONE);
            this.notifyDataSetChanged();
        }

        @Override
        public ShippingListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_shippinglist, parent, false);

            return new ShippingListViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final ShippingListViewHolder holder, final int position) {
            Shipping slRow = adapterShippingList.get(position);
            holder.textSLOrderNumber.setText(slRow.orderNumber);
            holder.textSLCustomerName.setText(slRow.name);
            holder.textSLCustomerAddress.setText(slRow.address);
            holder.btnSLView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentMain = new Intent(MainActivity.this,DetailShippingActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("current_order_number",adapterShippingList.get(position).orderNumber);
                    bundle.putInt("current_order_id",adapterShippingList.get(position).id);
                    intentMain.putExtras(bundle);
                    startActivity(intentMain);
                }
            });
            holder.btnSLSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentMain = new Intent(MainActivity.this,SubmitShippingActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("current_order_number",adapterShippingList.get(position).orderNumber);
                    bundle.putInt("current_order_id",adapterShippingList.get(position).id);
                    intentMain.putExtras(bundle);
                    startActivity(intentMain);
                }
            });
        }

        @Override
        public int getItemCount() {
            return adapterShippingList.size();
        }
    }
}
